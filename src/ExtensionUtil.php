<?php
declare(strict_types=1);

namespace Std\Ext;

use function extension_loaded;
use function phpversion;

/**
 * Class ExtensionUtil
 * @package Std\Ext
 * @since   1.0.0
 */
final class ExtensionUtil {
    /**
     * @param string $ext
     *
     * @return bool
     */
    public static function isLoaded(string $ext): bool {
        return extension_loaded($ext);
    }

    /**
     * @param string $ext
     *
     * @return string
     */
    public static function getVersion(string $ext): string {
        return phpversion($ext);
    }
}
